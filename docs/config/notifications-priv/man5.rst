:orphan:

======================
notifications-priv.cfg
======================

Description
===========

The file /etc/proxmox-backup/notifications-priv.cfg is a configuration file
for Proxmox Backup Server. It contains the configuration for the
notification system configuration.

File Format
===========

.. include:: format.rst

Options
=======

.. include:: config.rst

.. include:: ../../pbs-copyright.rst
