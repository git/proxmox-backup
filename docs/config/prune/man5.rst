:orphan:

=========
prune.cfg
=========

Description
===========

The file /etc/proxmox-backup/prune.cfg is a configuration file for Proxmox
Backup Server. It contains the prune job configuration.

File Format
===========

.. include:: format.rst

Options
=======

.. include:: config.rst

.. include:: ../../pbs-copyright.rst
