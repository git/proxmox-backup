:orphan:

========
node.cfg
========

Description
===========

The file /etc/proxmox-backup/node.cfg is a configuration file for Proxmox
Backup Server. It contains the general configuration regarding this node.

Options
=======

.. include:: format.rst

.. include:: ../../pbs-copyright.rst
