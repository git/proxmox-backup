:orphan:

================
verification.cfg
================

Description
===========

The file /etc/proxmox-backup/verification.cfg is a configuration file for
Proxmox Backup Server. It contains the verification job configuration.

File Format
===========

.. include:: format.rst

Options
=======

.. include:: config.rst

.. include:: ../../pbs-copyright.rst
